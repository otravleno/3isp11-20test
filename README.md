# 3ISP11-20Test



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- <b>[ERD- диаграмма] (https://gitlab.com/3isp1120/3isp11-20test/-/blob/main/ER-Diogramm.png)</b>
<b>![ER-Diogramm](./ER-Diogramm.png)</b>

-<b>[Use-case] (https://gitlab.com/3isp1120/3isp11-20test/-/blob/main/UseCase.png)</b>
<b>![UseCase](./UseCase.png)</b>

-<b>[Диагамма-последовательности] (https://gitlab.com/3isp1120/3isp11-20test/-/blob/main/%D0%94%D0%B8%D0%B0%D0%B3%D0%B0%D0%BC%D0%BC%D0%B0-_%D0%9F%D0%BE%D1%81%D0%BB%D0%B5%D0%B4%D0%BE%D0%B2%D0%B0%D1%82%D0%B5%D0%BB%D1%8C%D0%BD%D0%BE%D1%81%D1%82%D0%B8.png)</b>
<b>![Диагамма-последовательности](./Диагамма-_Последовательности.png)</b>
